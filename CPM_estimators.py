#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 02:17:11 2021

@author: shubham
"""

import numpy as np
import matplotlib.pyplot as plt
import colorednoise as cn
#import matplotlib as mpl
#from scipy.io import loadmat
#import pandas as pd
#from scipy.stats import pearsonr
t = np.arange(0,1,0.001)

def coef_fft(x,f):
    n = 1000
    fhat = np.fft.fft(x,n)
    R = 2*(np.real(fhat[f]/n))
    I = -2*(np.imag(fhat[f]/n))
    return R,I

def Estimated_stat(Ar_mean,Ar_std,Br_mean, Br_std,alpha):
    x2 = np.sin(2*np.pi*10*t+alpha)
    power_pl_10 = (np.abs(np.random.normal(Ar_mean,Ar_std,1000)))
    power_np_10 = (np.abs(np.random.normal(Br_mean,Br_std,1000)))
    phase_np = (np.random.uniform(-np.pi,np.pi,1000))
    np_pl_10 = np.array([])
    for i in range(1000):
        x3 = np.sin(2*np.pi*10*(t+phase_np[i]/(20*np.pi)))
        np_pl_10 = np.append(np_pl_10,power_np_10[i]*x3+power_pl_10[i]*x2+cn.powerlaw_psd_gaussian(1,1000))
    np_pl_10 = np_pl_10.reshape(1000,1000)
    # simulated Sr_t
        
    #calculating fft coefficients at 10Hz
    R_array = np.array([])
    I_array = np.array([])
    for ii in range(1000):
        r, i = coef_fft(np_pl_10[ii],10)
        R_array = np.append(R_array,r)
        I_array = np.append(I_array,i)
            
    est_alpha = np.arctan2(R_array.mean(0),I_array.mean(0))
    # find trials with positive phi_r
    
    z = R_array*np.cos(est_alpha)-I_array*np.sin(est_alpha)
    zz = z[np.where(z>0)]
    #etimates Ar_mean, Ar_std, Br_mean, Br_std
    # Ar estimate
    Ar_mean_est = I_array.mean(0)/np.cos(est_alpha)
    Ar_var_est = (((I_array**2).mean(0)-(R_array**2).mean(0))/np.cos(2*est_alpha)-Ar_mean_est**2)
    # Br estimate
    Br_mean_est = np.pi*0.5*(zz.mean(0))
    Br_var_est = 2*(((R_array**2).mean(0))*np.cos(est_alpha)**2-((I_array**2).mean(0))*np.sin(est_alpha)**2)/np.cos(2*est_alpha) - Br_mean_est**2



    return (Ar_mean_est, Ar_var_est, Br_mean_est, Br_var_est, est_alpha),(power_pl_10.mean(0),power_pl_10.var(0),power_np_10.mean(0),power_np_10.var(0), alpha)

br_mean_a = 5
br_std_a = 1
ar_mean_a = np.arange(1, 10 ,0.5)
ar_std_a = 1

Ar_mean_est_a = np.array([])
Ar_var_est_a = np.array([])
Br_mean_est_a = np.array([])
Br_var_est_a = np.array([])
alpha_est_array_a = np.array([])

Ar_mean_sim_a = np.array([])
Ar_var_sim_a = np.array([])
Br_mean_sim_a = np.array([])
Br_var_sim_a = np.array([])
alpha = 1


for i in ar_mean_a:
    est_a, sim_a = Estimated_stat(i,ar_std_a,br_mean_a,br_std_a,alpha)
    print(i)
    Ar_mean_est_a = np.append(Ar_mean_est_a,est_a[0])
    Ar_var_est_a = np.append(Ar_var_est_a,est_a[1])
    Br_mean_est_a = np.append(Br_mean_est_a,est_a[2])
    Br_var_est_a = np.append(Br_var_est_a,est_a[3])
    alpha_est_array_a = np.append(alpha_est_array_a,est_a[4])
    
    Ar_mean_sim_a = np.append(Ar_mean_sim_a,sim_a[0])
    Ar_var_sim_a = np.append(Ar_var_sim_a,sim_a[1])
    Br_mean_sim_a = np.append(Br_mean_sim_a,sim_a[2])
    Br_var_sim_a = np.append(Br_var_sim_a,sim_a[3])
######


br_mean_b = np.arange(1, 10 ,0.5)
br_std_b = 1
ar_mean_b = 4
ar_std_b = 1

Ar_mean_est_b = np.array([])
Ar_var_est_b = np.array([])
Br_mean_est_b = np.array([])
Br_var_est_b = np.array([])
alpha_est_array_b = np.array([])

Ar_mean_sim_b = np.array([])
Ar_var_sim_b = np.array([])
Br_mean_sim_b = np.array([])
Br_var_sim_b = np.array([])
alpha = 1


for i in br_mean_b:
    est_b, sim_b = Estimated_stat(ar_mean_b,ar_std_b,i,br_std_b,alpha)
    print(i)
    Ar_mean_est_b = np.append(Ar_mean_est_b,est_b[0])
    Ar_var_est_b = np.append(Ar_var_est_b,est_b[1])
    Br_mean_est_b = np.append(Br_mean_est_b,est_b[2])
    Br_var_est_b = np.append(Br_var_est_b,est_b[3])
    alpha_est_array_b = np.append(alpha_est_array_b,est_b[4])
    
    Ar_mean_sim_b = np.append(Ar_mean_sim_b,sim_b[0])
    Ar_var_sim_b = np.append(Ar_var_sim_b,sim_b[1])
    Br_mean_sim_b = np.append(Br_mean_sim_b,sim_b[2])
    Br_var_sim_b = np.append(Br_var_sim_b,sim_b[3])
#############
br_mean_c = 5
br_std_c = 1
ar_mean_c = 4
ar_std_c = np.arange(1, 10 ,0.5)

Ar_mean_est_c = np.array([])
Ar_var_est_c = np.array([])
Br_mean_est_c = np.array([])
Br_var_est_c = np.array([])
alpha_est_array_c = np.array([])

Ar_mean_sim_c = np.array([])
Ar_var_sim_c = np.array([])
Br_mean_sim_c = np.array([])
Br_var_sim_c = np.array([])
alpha = 1


for i in ar_std_c:
    est_c, sim_c = Estimated_stat(ar_mean_c,i,br_mean_c,br_std_c,alpha)
    print(i)
    Ar_mean_est_c = np.append(Ar_mean_est_c,est_c[0])
    Ar_var_est_c = np.append(Ar_var_est_c,est_c[1])
    Br_mean_est_c = np.append(Br_mean_est_c,est_c[2])
    Br_var_est_c = np.append(Br_var_est_c,est_c[3])
    alpha_est_array_c = np.append(alpha_est_array_c,est_c[4])
    
    Ar_mean_sim_c = np.append(Ar_mean_sim_c,sim_c[0])
    Ar_var_sim_c = np.append(Ar_var_sim_c,sim_c[1])
    Br_mean_sim_c = np.append(Br_mean_sim_c,sim_c[2])
    Br_var_sim_c = np.append(Br_var_sim_c,sim_c[3])
################

br_mean_d = 5
br_std_d = np.arange(1, 10 ,0.5)
ar_mean_d = 4
ar_std_d = 1

Ar_mean_est_d = np.array([])
Ar_var_est_d = np.array([])
Br_mean_est_d = np.array([])
Br_var_est_d = np.array([])
alpha_est_array_d = np.array([])

Ar_mean_sim_d = np.array([])
Ar_var_sim_d = np.array([])
Br_mean_sim_d = np.array([])
Br_var_sim_d = np.array([])
alpha = 1


for i in br_std_d:
    est_d, sim_d = Estimated_stat(ar_mean_d,ar_std_d,br_mean_d,i,alpha)
    print(i)
    Ar_mean_est_d = np.append(Ar_mean_est_d,est_d[0])
    Ar_var_est_d = np.append(Ar_var_est_d,est_d[1])
    Br_mean_est_d = np.append(Br_mean_est_d,est_d[2])
    Br_var_est_d = np.append(Br_var_est_d,est_d[3])
    alpha_est_array_d = np.append(alpha_est_array_d,est_d[4])
    
    Ar_mean_sim_d = np.append(Ar_mean_sim_d,sim_d[0])
    Ar_var_sim_d = np.append(Ar_var_sim_d,sim_d[1])
    Br_mean_sim_d = np.append(Br_mean_sim_d,sim_d[2])
    Br_var_sim_d = np.append(Br_var_sim_d,sim_d[3])


fig,((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2,ncols=2,figsize=(14,8),gridspec_kw={'wspace':0.5,'hspace':0.5})
fs = 20
ax1.scatter(Ar_mean_sim_a,Ar_mean_est_a, s=75, edgecolors='k',label='estimated values')
ax1.plot(Ar_mean_sim_a,Ar_mean_sim_a, label='x=y')
ax1.set_title('PL amplitude', fontsize = fs)
ax1.set_xlabel('Simulated Mean $A_r$', fontsize = fs)
ax1.set_ylabel('Estimated Mean $A_r$', fontsize = fs)
#ax1.legend()

ax2.scatter(Br_mean_sim_b,Br_mean_est_b, s=75, edgecolors='k',label='estimated values')
ax2.plot(Br_mean_sim_b,Br_mean_sim_b, label='x=y')
ax2.set_title('NPL amplitude', fontsize = fs)
ax2.set_xlabel('Simulated Mean $B_r$', fontsize = fs)
ax2.set_ylabel('Estimated Mean $B_r$', fontsize = fs)
#ax2.legend()

ax3.scatter(Ar_var_sim_c,Ar_var_est_c, s=75, edgecolors='k',label='estimated values')
ax3.plot(Ar_var_sim_c,Ar_var_sim_c, label='x=y')
ax3.set_title('PL amplitude variance', fontsize = fs)
ax3.set_xlabel('Simulated $A_r$ variance', fontsize = fs)
ax3.set_ylabel('Estimated $A_r$ variance', fontsize = fs)
#ax3.legend()

ax4.scatter(Br_var_sim_d,Br_var_est_d, s=75, edgecolors='k',label='estimated values')
ax4.plot(Br_var_sim_d,Br_var_sim_d, label='x=y')
ax4.set_title('NPL amplitude variance', fontsize = fs)
ax4.set_xlabel('Simulated $B_r$ variance', fontsize = fs)
ax4.set_ylabel('Estimated $B_r$ variance', fontsize = fs)
#ax4.legend()
#
#plt.scatter(Ar_mean_sim,Ar_mean_est, label='estimated values')
#plt.plot(Ar_mean_sim,Ar_mean_sim, label='x=y')
#plt.legend()
#plt.xlabel('Simulated Ar mean')
#plt.ylabel('Estimated Ar mean')
#plt.title('Phase locked Amplitude')





