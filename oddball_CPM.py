#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 15:04:48 2022

@author: shubham singhal
"""

import numpy as np
import matplotlib.pyplot as plt
 
#rom scipy.io import loadmat
#import pandas as pd
import mat73
data = mat73.loadmat('/home/priyanka/Downloads/shubham_oddball2.mat')
odd_aud = data['st_a'].T
stand_aud = data['wt_a'].T

def coef_fft(x,f):
    n = len(x)
    fhat = np.fft.fft(x,n)
    f_i = int(f*len(x)/1000)
    R = 2*(np.real(fhat[f_i]/n))
    I = -2*(np.imag(fhat[f_i]/n))
    return R,I
freq = np.fft.fftfreq(500,0.001)
# 4 all electrodes, oddball audio
plt.figure(figsize=(13,6))
fs=24
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=15)
for elec in np.arange(64):
    var_Ar_freq_odd = np.array([])
    for f in freq[1:26]:
        R_stand_array = np.array([])
        I_stand_array = np.array([])
        for ii in range(len(odd_aud)):
            r,i = coef_fft(odd_aud[ii,elec,:],f)
            R_stand_array = np.append(R_stand_array,r)
            I_stand_array = np.append(I_stand_array,i)
        alpha = np.arctan2(R_stand_array.mean(0),I_stand_array.mean(0))
        sqr_Ar = ((I_stand_array**2).mean(0)-(R_stand_array**2).mean(0))/np.cos(2*alpha)
    
        Br_sqr = 2*((R_stand_array**2).mean(0)*(np.cos(alpha))**2-(I_stand_array**2).mean(0)*(np.sin(alpha))**2)/np.cos(2*alpha)
        mean_Ar = I_stand_array.mean(0)/np.cos(alpha)
        var_Ar = (sqr_Ar - mean_Ar**2)
        var_Ar_freq_odd = np.append(var_Ar_freq_odd,np.log(abs(var_Ar/Br_sqr)))
        print(abs(var_Ar), f)
    color_arr = np.array([])
    for jj in var_Ar_freq_odd:
        if jj<-0.69:
            c = 'grey'
        else:
            c= 'aqua'
        color_arr = np.append(color_arr,c)    
    plt.scatter(freq[1:26],var_Ar_freq_odd,label=elec, c = color_arr,s=100,edgecolors='k')
plt.xlabel('Frequency(Hz)', fontsize = fs)
plt.ylabel(r'log($ \beta $)', fontsize = fs)
plt.title('Leaking parameter for all electrodes', fontsize=fs)
#plt.legend()
plt.show()