#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 28 22:55:29 2021

@author: shubham
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
#from scipy.io import loadmat
#import pandas as pd
from scipy.stats import pearsonr
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42
mpl.rcParams['font.family'] = 'Arial'

t = np.arange(0,1,0.001)

# condition 1
x2 = np.sin(2*np.pi*10*t)
power_pl_10 = (np.abs(np.random.normal(3,50,1000)))
power_np_10 = (np.abs(np.random.normal(3,20,1000)))
phase_np = (np.random.uniform(-np.pi,np.pi,1000))
np_pl_10 = np.array([])
for i in range(1000):
    x3 = np.sin(2*np.pi*10*(t+phase_np[i]/(20*np.pi)))
    np_pl_10 = np.append(np_pl_10,power_np_10[i]*x3+power_pl_10[i]*x2)
np_pl_10 = np_pl_10.reshape(1000,1000)

erp = np_pl_10.mean(0)

NPL = np_pl_10-erp

def power(x,f):
    fhat = np.fft.fft(x,len(x))
    freq = np.fft.fftfreq(len(x),0.001)
    PSD = 4*fhat*np.conj(fhat)/(len(x))**2
    return np.real(PSD[np.where(freq==f)])

npl_power = np.array([])
for i in range(1000):
    p = power(NPL[i],10)
    npl_power = np.append(npl_power,p)
    

pl_power = np.array([])
for i in range(1000):
    tp = power(np_pl_10[i],10)
    plp = tp-npl_power[i]
    pl_power = np.append(pl_power,plp)

fig,(ax1, ax2) = plt.subplots(nrows=2,ncols=1,figsize=(8,8),gridspec_kw={'wspace':0.5,'hspace':0.5})
fs = 15
#plt.figure(figsize=(15,6))
ax2.scatter(power_np_10**2,npl_power, c=np.random.rand(1000), s=75, edgecolors='k')
ax2.set_title('Non-phase-locked power', fontsize = fs)
ax2.set_xlabel('Simulated NPL power', fontsize = fs)
ax2.set_xlim(0,4500)
ax2.set_ylabel('Estimated NPL power', fontsize = fs)
ax1.scatter(power_pl_10**2,pl_power, c=np.random.rand(1000), s=75, edgecolors='k')
ax1.set_title('Phase-locked power', fontsize = fs)
ax1.set_xlabel('Simulated PL power', fontsize = fs)
ax1.set_xlim(0,5200)
ax1.set_ylabel('Estimated PL power', fontsize = fs)

print(pearsonr(power_np_10**2,npl_power))
print(pearsonr(power_pl_10**2,pl_power))





