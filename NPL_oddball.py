#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  8 10:58:07 2022

@author: shubham Singhal=
"""
import numpy as np
import matplotlib.pyplot as plt
 
#from scipy.io import loadmat
#import pandas as pd
import mat73
data = mat73.loadmat('/home/priyanka/Downloads/shubham_oddball2.mat')

odd_aud = data['st_a'].T
stand_aud = data['wt_a'].T

def coef_fft(x,f):
    n = len(x)
    fhat = np.fft.fft(x,n)
    f_i = np.where(np.fft.fftfreq(n,0.001)==f)
    R = 2*(np.real(fhat[f_i]/n))
    I = -2*(np.imag(fhat[f_i]/n))
    return R,I

# @freq 2Hz
f = 2
beta_arr = np.array([])
npl_cpm = np.array([])
npl_av_arr = np.array([])
pl_cpm = np.array([])
pl_av_arr = np.array([])
for elec in np.arange(64):
    print(elec)
    R_array = np.array([])
    I_array = np.array([])
    for ii in range(len(odd_aud)):
        r, i = coef_fft(odd_aud[ii,elec,:],f)
        R_array = np.append(R_array,r)
        I_array = np.append(I_array,i)

    est_alpha = np.arctan2(R_array.mean(0),I_array.mean(0))
    z = R_array*np.cos(est_alpha)-I_array*np.sin(est_alpha)
    zz = z[np.where(z>0)]

    #etimates Ar_mean, Ar_std, Br_mean, Br_std
    # Ar estimate
    Ar_mean_est = I_array.mean(0)/np.cos(est_alpha)
    Ar_var_est = (((I_array**2).mean(0)-(R_array**2).mean(0))/np.cos(2*est_alpha)-Ar_mean_est**2)
    Ar_sqr_mean = ((I_array**2).mean(0)-(R_array**2).mean(0))/np.cos(2*est_alpha)
    # Br estimate
    Br_mean_est = np.pi*0.5*(zz.mean(0))
    Br_var_est = 2*(((R_array**2).mean(0))*np.cos(est_alpha)**2-((I_array**2).mean(0))*np.sin(est_alpha)**2)/np.cos(2*est_alpha) - Br_mean_est**2
    Br_sqr_mean =(2*(((R_array**2).mean(0))*np.cos(est_alpha)**2-((I_array**2).mean(0))*np.sin(est_alpha)**2)/np.cos(2*est_alpha)) 
    beta = (Ar_var_est)/Br_sqr_mean


    # Av_estimates
    erp = odd_aud[:,elec,:].mean(0)
    sr_erp = odd_aud[:,elec,:]-erp
    
    npl_power = np.array([])
    pl_power = np.array([])
    for ii in range(len(odd_aud)):
        r,i = coef_fft(sr_erp[ii,:],f)
        r2,i2 = coef_fft(erp,f)
        
        tp = r2**2+i2**2
        npl = r**2+i**2
        npl_power = np.append(npl_power, npl)
        pl_power = np.append(pl_power,tp-npl)
    npl_av = npl_power.mean(0)
    pl_av = pl_power.mean(0)
    
    beta_arr = np.append(beta_arr,beta)
    npl_cpm = np.append(npl_cpm,Br_sqr_mean)
    npl_av_arr = np.append(npl_av_arr,npl_av)
    pl_cpm = np.append(pl_cpm,Ar_sqr_mean)
    pl_av_arr = np.append(pl_av_arr,pl_av)
plt.figure(figsize=(13,6))
fs=24
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=15)   

plt.scatter(np.log(beta_arr),np.log(npl_cpm), color='r',label='CPM NPL power estimate')
plt.scatter(np.log(beta_arr),np.log(npl_av_arr),label='Averaging method NPL power estimate')
plt.xlabel(r'log($\beta$)', fontsize = fs)
plt.ylabel('log(NPL power)', fontsize = fs)
plt.title('NPL power at 48 Hz for different electrodes', fontsize=fs)
plt.legend()
plt.show()

