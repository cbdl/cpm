#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 11:01:41 2022

@author: shubham singhal

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

# defining time window and sampling rate
t = np.arange(0,2,0.001)

# simulating signal as only PL power for (0-0.5sec), only NPL power for (0.5-1sec),
# simulataneous PL and NPL power for (1-1.5sec), simulataneous PL and NPL power for (1.5-2sec) but 2 times amplitude w.r.t (1-1.5sec)   
x2 = np.sin(2*np.pi*10*t)
x2[500:1000]=0
power_pl_0 = 1.5*np.random.uniform(0.00,0.25,665)
power_np_0 = np.random.uniform(0.1,0.15,665)
#power_pl_2 = 2*np.random.uniform(0.00,0.25,665)
#power_np_2 = 2*np.random.uniform(0.1,0.15,665)
phase_np = np.random.uniform(-np.pi,np.pi,665)
signal = np.array([])
for i in range(665):
    x3 = np.sin(2*np.pi*10*t+phase_np[i])
    x3[t<0.50]
    s = np.concatenate((power_pl_0[i]*x2[0:500],power_np_0[i]*x3[500:1000],power_pl_0[i]*x2[1000:1500]+power_np_0[i]*x3[1000:1500],2*power_pl_0[i]*x2[1500:2000]+2*power_np_0[i]*x3[1500:2000]))
    signal = np.append(signal,s)
signal = signal.reshape(665,2000)

# single-trial plot
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.title('Single trial EEG activity: $S_r(t)$', fontsize = 35)
plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Voltage(μV)', fontsize = 24)
plt.plot(t,signal[11,:], lw =5)

#ERP
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.title(r'Trial averaged activity $\langle S_r(t) \rangle $', fontsize = 35)
plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Voltage(μV)', fontsize = 24)
plt.plot(t,signal.mean(0), lw =5)
######################
# Gabor transform to power spectral density
def PSD_gab(data):
    dt = 0.001
    t = np.arange(0,2, dt)
    tslide = np.arange(0,2,0.01)
    n =2000
    Sgt_spec = np.array([])
    for j in range(len(tslide)):
        g = np.exp(-75*(t-tslide[j])**2)
        Sg = g*data
        fhat = np.fft.fft(Sg,n)
        PSD = (2*abs(fhat)/n)**2
        Sgt_spec = np.append(Sgt_spec,PSD[0:50])
    Sgt_spec = Sgt_spec.reshape(len(tslide),50)
    return Sgt_spec

tslide = np.arange(0,2,0.01)
# Total Power PSD for all trials
PSD_total = np.array([])
for i in range(665):
    p = PSD_gab(signal[i,:])
    PSD_total = np.append(PSD_total,p)
PSD_total = PSD_total.reshape(665,200,50)

#erp substracted trials
np_signal = signal-signal.mean(0)

# NPL power
PSD_np = np.array([])
for i in range(665):
    p = PSD_gab(np_signal[i,:])
    PSD_np = np.append(PSD_np,p)
PSD_np = PSD_np.reshape(665,200,50)

TP_noise = PSD_total #/PSD_noise.mean(0){in case of relative to baseline power}
NP_noise = PSD_np #/PSD_baseline.mean(0)
Pl_noise = TP_noise-NP_noise

# frequency array corresponding to fft
freq = np.fft.fftfreq(2000,0.001)

# plot total power 
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.pcolor(tslide,freq[0:50],PSD_total.mean(0).T,cmap='hot', shading ='auto', vmax=0.001, vmin =0.000001)
plt.title('TP ', fontsize=35 )
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Frequency(Hz)', fontsize = 24)

plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)
#plt.colorbar()
plt.show()

# plot Non-phase-locked power
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.pcolor(tslide,freq[0:50],NP_noise.mean(0).T,cmap='hot', shading ='auto', vmax=0.0007, vmin =0.000001)
plt.title('NPL power \n TF($NPL_r$):TF{$S_r(t)$-ERP(t)} ', fontsize=35 )
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Frequency(Hz)', fontsize = 24)
plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)

#plt.colorbar()
plt.show()

#plot phase-locked power 
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.pcolor(tslide,freq[0:50],Pl_noise.mean(0).T,cmap='hot', shading ='auto',vmax=0.0007, vmin =0.000001)
plt.title('PL power \n TF($PL_r$):TF{$S_r(t)$}-TF{$NPL_r$} ', fontsize=35 )
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Frequency(Hz)', fontsize = 24)

plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)
#plt.colorbar()
plt.show()

#plot Pl, NPL power at 10Hz versus time via averaging method
plt.figure(figsize=(13,6))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.plot(tslide,100*NP_noise.mean(0)[:,20], label='NPL power', lw =5)
plt.plot(tslide,100*Pl_noise.mean(0)[:,20], label = 'PL power', lw =5, color='r')
#plt.legend(prop={'size' :20})
plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)
#plt.title('Power at 10Hz', fontsize = 35)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Power', fontsize = 24) 

#############################################
#PLV METHOD

from scipy.signal import hilbert
# phase-time series of all trials 
theta_ar = np.array([])

for i in range(665):
    analytic_s = hilbert(signal[i,:])
    inst_phase = np.unwrap(np.angle(analytic_s))
    theta_ar = np.append(theta_ar,inst_phase)
theta_ar = theta_ar.reshape(665,2000) 

# defining PLV
def PLV(theta_ar):
    plv = 0
    for i in range(len(theta_ar)):
        plv +=np.exp(np.complex(0,theta_ar[i]))
    return abs(plv)/len(theta_ar)

# plv-t series
plv_t = np.array([])
for i in range(2000):
    plv = PLV(theta_ar[:,i])
    plv_t = np.append(plv_t,plv)

# plot for single-trial phase time-series
plt.figure(figsize=(10,5))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.plot(t, np.angle(hilbert(signal[131,:])), lw = 4)
plt.title('Single trial phase time-series', fontsize = 35)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Phase(rad)', fontsize = 24)
plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)

# PLV versus time plot for 10Hz
plt.figure(figsize=(13,6))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.plot(t,plv_t, lw = 5, color='k')
#plt.title('Phase-locked value at 10Hz', fontsize = 35)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Phase-locked value(PLV)', fontsize = 24)

plt.locator_params(axis='y', nbins =4)
plt.locator_params(axis='x', nbins =6)

###########################################################
#CPM_method

#snipping signal into four time windows
signal_1 = signal[:,0:500]
signal_2 = signal[:,500:1000]
signal_3 = signal[:,1000:1500]
signal_4 = signal[:,1500:2000]

# fft to get Fourier coeffcient at 10Hz and Pl and NPL power calculationn via CPM
R_array_1 = np.array([])
I_array_1 = np.array([])
f = 10
for j in range(len(signal_1)):
    r,i = coef_fft(signal_1[j,:],f)
    R_array_1= np.append(R_array_1,r)
    I_array_1= np.append(I_array_1,i)
    
alpha_1 = np.arctan2(R_array_1.mean(0),I_array_1.mean(0))
sqr_Ar_1 = ((I_array_1**2).mean(0)-(R_array_1**2).mean(0))/np.cos(2*alpha_1)
Br_sqr_1 = 2*((R_array_1**2).mean(0)*(np.cos(alpha_1))**2-(I_array_1**2).mean(0)*(np.sin(alpha_1))**2)/np.cos(2*alpha_1)
    
mean_Ar_1 = I_array_1.mean(0)/np.cos(alpha_1)
var_Ar_1 = (sqr_Ar_1 - mean_Ar_1**2)
beta_1 = (var_Ar_1/Br_sqr_1)

##
R_array_2 = np.array([])
I_array_2 = np.array([])
f = 10
for j in range(len(signal_2)):
    r,i = coef_fft(signal_2[j,:],f)
    R_array_2= np.append(R_array_2,r)
    I_array_2= np.append(I_array_2,i)
    
alpha_2 = np.arctan2(R_array_2.mean(0),I_array_2.mean(0))
sqr_Ar_2 = ((I_array_2**2).mean(0)-(R_array_2**2).mean(0))/np.cos(2*alpha_2)
Br_sqr_2 = 2*((R_array_2**2).mean(0)*(np.cos(alpha_2))**2-(I_array_2**2).mean(0)*(np.sin(alpha_2))**2)/np.cos(2*alpha_2)
    
mean_Ar_2 = I_array_2.mean(0)/np.cos(alpha_2)
var_Ar_2 = (sqr_Ar_2 - mean_Ar_2**2)
beta_2 = (var_Ar_2/Br_sqr_1)
##
R_array_3 = np.array([])
I_array_3 = np.array([])
f = 10
for j in range(len(signal_3)):
    r,i = coef_fft(signal_3[j,:],f)
    R_array_3= np.append(R_array_3,r)
    I_array_3= np.append(I_array_3,i)
    
alpha_3 = np.arctan2(R_array_3.mean(0),I_array_3.mean(0))
sqr_Ar_3 = ((I_array_3**2).mean(0)-(R_array_3**2).mean(0))/np.cos(2*alpha_3)
Br_sqr_3 = 2*((R_array_3**2).mean(0)*(np.cos(alpha_3))**2-(I_array_3**2).mean(0)*(np.sin(alpha_3))**2)/np.cos(2*alpha_3)
    
mean_Ar_3 = I_array_3.mean(0)/np.cos(alpha_3)
var_Ar_3 = (sqr_Ar_3 - mean_Ar_3**2)
beta_3 = (var_Ar_3/Br_sqr_3)
##
R_array_4 = np.array([])
I_array_4 = np.array([])
f = 10
for j in range(len(signal_4)):
    r,i = coef_fft(signal_4[j,:],f)
    R_array_4= np.append(R_array_4,r)
    I_array_4= np.append(I_array_4,i)
    
alpha_4 = np.arctan2(R_array_4.mean(0),I_array_4.mean(0))
sqr_Ar_4 = ((I_array_4**2).mean(0)-(R_array_4**2).mean(0))/np.cos(2*alpha_4)
Br_sqr_4 = 2*((R_array_4**2).mean(0)*(np.cos(alpha_4))**2-(I_array_4**2).mean(0)*(np.sin(alpha_4))**2)/np.cos(2*alpha_4)
    
mean_Ar_4 = I_array_4.mean(0)/np.cos(alpha_4)
var_Ar_4 = (sqr_Ar_4 - mean_Ar_4**2)
beta_4 = (var_Ar_4/Br_sqr_4)

##
# ploting power estimates (estimated via CPM)
# pl power 
plt.figure(figsize=(13,6))
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=20)
plt.plot(t,np.concatenate((Br_sqr_1*np.ones(500),Br_sqr_2*np.ones(500),Br_sqr_3*np.ones(500),Br_sqr_4*np.ones(500))), label = 'NPL power', lw = 5)
plt.plot(t,np.concatenate((sqr_Ar_1*np.ones(500),sqr_Ar_2*np.ones(500),sqr_Ar_3*np.ones(500),sqr_Ar_4*np.ones(500))), label='PL power', lw = 5, color = 'r')
#plt.legend(prop={'size' :20})
#plt.title('Power at 10Hz', fontsize = 35)
plt.xlabel('Time(s)', fontsize = 24)
plt.ylabel('Power', fontsize = 24)
plt.locator_params(axis='y', nbins =5)
plt.locator_params(axis='x', nbins =6)



