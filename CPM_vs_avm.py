#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 04:49:45 2021

@author: Shubham
"""
import numpy as np
import matplotlib.pyplot as plt
import colorednoise as cn
#import matplotlib as mpl
#from scipy.io import loadmat
#import pandas as pd
#from scipy.stats import pearsonr
t = np.arange(0,1,0.001)

def coef_fft(x,f):
    n = 1000
    fhat = np.fft.fft(x,n)
    R = 2*(np.real(fhat[f]/n))
    I = -2*(np.imag(fhat[f]/n))
    return R,I

def Estimated_stat(Ar_mean,Ar_std,Br_mean, Br_std,alpha):
    x2 = np.sin(2*np.pi*10*t+alpha)
    power_pl_10 = (np.abs(np.random.normal(Ar_mean,Ar_std,1000)))
    power_np_10 = (np.abs(np.random.normal(Br_mean,Br_std,1000)))
    phase_np = (np.random.uniform(-np.pi,np.pi,1000))
    np_pl_10 = np.array([])
    for i in range(1000):
        x3 = np.sin(2*np.pi*10*(t+phase_np[i]/(20*np.pi)))
        np_pl_10 = np.append(np_pl_10,power_np_10[i]*x3+power_pl_10[i]*x2+cn.powerlaw_psd_gaussian(1,1000))
    np_pl_10 = np_pl_10.reshape(1000,1000)
    # simulated Sr_t
        
    #calculating fft coefficients at 10Hz
    R_array = np.array([])
    I_array = np.array([])
    for ii in range(1000):
        r, i = coef_fft(np_pl_10[ii],10)
        R_array = np.append(R_array,r)
        I_array = np.append(I_array,i)
            
    est_alpha = np.arctan2(R_array.mean(0),I_array.mean(0))
    # find trials with positive phi_r
    
    z = R_array*np.cos(est_alpha)-I_array*np.sin(est_alpha)
    zz = z[np.where(z>0)]
    #etimates Ar_mean, Ar_std, Br_mean, Br_std
    # Ar estimate
    Ar_mean_est = I_array.mean(0)/np.cos(est_alpha)
    Ar_var_est = (((I_array**2).mean(0)-(R_array**2).mean(0))/np.cos(2*est_alpha)-Ar_mean_est**2)
    # Br estimate
    Br_mean_est = np.pi*0.5*(zz.mean(0))
    Br_var_est = 2*(((R_array**2).mean(0))*np.cos(est_alpha)**2-((I_array**2).mean(0))*np.sin(est_alpha)**2)/np.cos(2*est_alpha) - Br_mean_est**2
    
    # Av_estimates
    erp = np_pl_10.mean(0)
    sr_erp = np_pl_10-erp
    
    npl_power = np.array([])
    pl_power = np.array([])
    for ii in range(1000):
        r,i = coef_fft(sr_erp[ii,:],10)
        r2,i2 = coef_fft(np_pl_10[ii,:],10)
        
        tp = r2**2+i2**2
        npl = r**2+i**2
        npl_power = np.append(npl_power, npl)
        pl_power = np.append(pl_power,tp-npl)
        
    sim_npl_power_mean = (power_np_10**2).mean(0)
    sim_pl_power_mean = (power_pl_10**2).mean(0)
    sim_npl_power_std = (power_np_10**2).std(0)
    sim_pl_power_std = (power_pl_10**2).std(0)

    
    return ((Ar_mean_est, Ar_var_est, Br_mean_est, Br_var_est, est_alpha),(power_pl_10.mean(0),power_pl_10.var(0),power_np_10.mean(0),power_np_10.var(0), alpha)),((pl_power.mean(0),pl_power.std(0),npl_power.mean(0),npl_power.std(0)),(sim_pl_power_mean,sim_pl_power_std,sim_npl_power_mean,sim_npl_power_std))

br_mean = 5
br_std = 5
ar_mean = 4
ar_std = np.arange(1, 20 ,0.5)

Ar_mean_est = np.array([])
Ar_var_est = np.array([])
Br_mean_est = np.array([])
Br_var_est = np.array([])
alpha_est_array = np.array([])

Ar_mean_sim = np.array([])
Ar_var_sim = np.array([])
Br_mean_sim = np.array([])
Br_var_sim = np.array([])
alpha = 1

Ar2_mean_est = np.array([])
Ar2_std_est = np.array([])
Br2_mean_est = np.array([])
Br2_std_est = np.array([])


Ar2_mean_sim = np.array([])
Ar2_std_sim = np.array([])
Br2_mean_sim = np.array([])
Br2_std_sim = np.array([])

for i in ar_std:
    MV_stat, Aver_stat = Estimated_stat(ar_mean,i,br_mean,br_std,alpha)
    est, sim = MV_stat
    est2,sim2 = Aver_stat
    print(i)
    Ar_mean_est = np.append(Ar_mean_est,est[0])
    Ar_var_est = np.append(Ar_var_est,est[1])
    Br_mean_est = np.append(Br_mean_est,est[2])
    Br_var_est = np.append(Br_var_est,est[3])
    alpha_est_array = np.append(alpha_est_array,est[4])
    
    Ar_mean_sim = np.append(Ar_mean_sim,sim[0])
    Ar_var_sim = np.append(Ar_var_sim,sim[1])
    Br_mean_sim = np.append(Br_mean_sim,sim[2])
    Br_var_sim = np.append(Br_var_sim,sim[3])
    
    Ar2_mean_est = np.append(Ar2_mean_est,est2[0])
    Ar2_std_est = np.append(Ar2_std_est,est2[1])
    Br2_mean_est = np.append(Br2_mean_est,est2[2])
    Br2_std_est = np.append(Br2_std_est,est2[3])
    
    Ar2_mean_sim = np.append(Ar2_mean_sim,sim2[0])
    Ar2_std_sim = np.append(Ar2_std_sim,sim2[1])
    Br2_mean_sim = np.append(Br2_mean_sim,sim2[2])
    Br2_std_sim = np.append(Br2_std_sim,sim2[3])    
    
sqr_br_mean_MV_est = Br_var_est+Br_mean_est**2

'''
plt.figure(figsize=(13,6))
fs=24
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=15)
plt.scatter(Ar_var_sim,sqr_br_mean_MV_est, label='CPM NPL power estimate', s=100,edgecolors='k')
plt.scatter(Ar_var_sim,Br2_mean_est, label='Averaging method NPL power estimate',s=100,edgecolors='k')
plt.scatter(Ar_var_sim,Br2_mean_sim, label='Simulated NPL power ', color='g', linewidth = 4)
plt.xlabel('Variance $A_r$', fontsize = fs)
plt.ylabel('NPL power estimate', fontsize = fs)
plt.legend( fontsize = 15)'''

plt.figure(figsize=(13,6))
fs=24
plt.tick_params(axis='both',which='major',direction='in',length=10,width=5,color='black',pad=15,labelsize=15)
plt.scatter(Ar_var_sim/Br2_mean_sim,sqr_br_mean_MV_est, label='CPM NPL power estimate', s=100,edgecolors='k')
plt.scatter(Ar_var_sim/Br2_mean_sim,Br2_mean_est, label='Averaging method NPL power estimate',s=100,edgecolors='k')
plt.plot(Ar_var_sim/Br2_mean_sim,Br2_mean_sim, label='Simulated NPL power ', color='g', linewidth = 4)
plt.xlabel(r'Variance($A_r) / \langle B^2_r \rangle $[i.e.,$\beta$]', fontsize = fs)
plt.ylabel('NPL power estimate', fontsize = fs)
plt.legend( fontsize = 15)
plt.show()


